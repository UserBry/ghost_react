import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./Home";
import Post from "./Post";

class App extends Component {
    render() {
      return (
            <Switch>
                <Route
                  exact path={'/'} render={
                    props => (
                      <Home 
                        // {...props} 
                      />)
                  }
                />
                <Route path="/post" render={
                    props => (
                      <Post
                        {...props}
                      />)
                  }
                />
            </Switch>
      )
    }
}

export default App;